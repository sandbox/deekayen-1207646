<?php

/**
 * @file
 * Demonstration of IP authentication callback. Please don't use it in production. The access callback SQL isn't properly escaped.
 */

/**
 * Implementation of hook_menu().
 *
 * @return array
 */
function ipauth_helloworld_menu() {
  $links['ipauth_helloworld'] = array(
    'title' => 'Hello world!',
    'page callback' => 'ipauth_helloworld_text',
    'access callback' => 'ipauth_helloworld_access',
    'type' => MENU_CALLBACK
  );
  $links['admin/settings/ipauth_helloworld'] = array(
    'title' => 'Hello world!',
    'description' => 'Set the content type and fields that contain the access control information based on IP address.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ipauth_helloworld_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM
  );
  return $links;
}

/**
 * Menu callback to determine if the IP address of the requestor is allowed.
 *
 * @return boolean TRUE if IP was found
 */
function ipauth_helloworld_access() {
 // without the sprintf(), ip2long() would return a negative integer
  // where INET_ATON() would return a positive one on 172.16.254.254
  // http://us2.php.net/manual/en/function.ip2long.php#47341
  $longintip = sprintf("%u", ip2long(ip_address()));
  // using %d in the query replacement will result in 2147483647 being the replacement
  // for large unsigned integers instead of the proper IP. Use something instead that
  // allows for large unsigned integers.
  $table = 'content_type_'. variable_get('ipauth_helloworld_content_type', '');
  $lower_field = variable_get('ipauth_helloworld_lower_ip_field', '') .'_value';
  $higher_field = variable_get('ipauth_helloworld_higher_ip_field', '') .'_value';
  $sql = "SELECT COUNT(n.nid) FROM {%s} AS a INNER JOIN {node} AS n ON a.nid = n.nid AND a.vid = n.vid WHERE INET_ATON(a.%s) <= %f AND INET_ATON(a.%s) >= %f AND n.status = 1";
  $result = db_result(db_query_range($sql, $table, $lower_field, $longintip, $higher_field, $longintip, 0, 1));
  if ($result > 0) {
    return TRUE;
  }
  return FALSE;
}

function ipauth_helloworld_text() {
  print theme('page', t('Hello world!'));
  exit;
}

function ipauth_helloworld_admin_settings() {
  $content_types = node_get_types('names');
  $form['ipauth_helloworld_content_type'] = array(
    '#title' => t('Content type'),
    '#type' => 'select',
    '#options' => $content_types,
    '#default_value' => variable_get('ipauth_helloworld_content_type', ''),
    '#required' => TRUE
  );
  $result = db_query('SELECT field_name, label FROM {content_node_field_instance} ORDER BY label');
  while ($row = db_fetch_object($result)) {
    $content_fields[$row->field_name] = "$row->label ($row->field_name)";
  }
  $form['ipauth_helloworld_lower_ip_field'] = array(
    '#title' => t('Lower IP'),
    '#type' => 'select',
    '#options' => $content_fields,
    '#default_value' => variable_get('ipauth_helloworld_lower_ip_field', ''),
    '#required' => TRUE
  );
  $form['ipauth_helloworld_higher_ip_field'] = array(
    '#title' => t('Higher IP'),
    '#type' => 'select',
    '#options' => $content_fields,
    '#default_value' => variable_get('ipauth_helloworld_higher_ip_field', ''),
    '#required' => TRUE
  );
  return system_settings_form($form);
}
